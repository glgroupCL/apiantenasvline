let bodyParser = require('body-parser')
let cors = require('cors')

var express = require('express');
var mysql = require("mysql");
var app = express();

let Principal = require('./controller/PrincipalController')
let Seguridad = require('./controller/SeguridadController')
let EnvioCorreo = require('./controller/EnvioCorreoInventario')
 
let config = require('./config')

const sqlsp = require('sqlstoreprocedure')
sp = new sqlsp(config.dbconexionSQL.user, config.dbconexionSQL.host, config.dbconexionSQL.dbname, config.dbconexionSQL.pass);
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
	extended: true
}));
app.use(cors())

app.use(function(req, res, next) {
	res.setHeader('Access-Control-Allow-Origin', '*')
	res.setHeader('Access-Control-Allow-Methods', 'POST,GET')
	res.setHeader('Content-Type', 'application/json')
	next()
});
 
let rutas = express.Router()
rutas.post('/', function (req, res, next) {
    res.send({
		'Mensaje': 'Bienvenido a la API REST!'
	})
});

rutas.route("/sp").post(Principal.ValidaDatos)

 
app.use(rutas)
app.listen(config.puerto, function () {
    console.log('Servicio corriendo en http://localhost:'+config.puerto);
});
