
let nodemailer = require("nodemailer")
let fs = require('fs')
let Seguridad = require('./SeguridadController')
let config = require('../config')

var excel = require ( 'excel4node' );

exports.enviaCorreo = (req, res) => {
    console.log(req.body.item)
    console.log(req.body.cantidad)
    let item = req.body.item.split(",")  
    let cantidad = req.body.cantidad.split(",")  
    let sucursal = req.body.sucursal;
    let correo = req.body.correo;

    const pathExcel = 'C:\\prueba\\inventario_'+sucursal+'.xlsx'
    const nombreExcel = `inventario`+sucursal+`.xlsx`

    if(fs.existsSync(pathExcel)) {
		fs.unlink(pathExcel, (err)=>{ 
			if(err) throw err;
		})
	}

    var workbook =  new excel.Workbook ();
	var worksheet = workbook.addWorksheet ( 'ReporteInventario' );
    var j = 1;
    var x = 2;
    worksheet.cell(1, 1).string("PRODUCTO");
	worksheet.cell(1, 2).string("CANTIDAD");

    for (var i = 0; i < item.length; i++) {
        
        worksheet.cell(x, 1).string(item[i]); 
        worksheet.cell(x, 2).string(cantidad[i]);          
        x++;
        
       
    }

    workbook.write (`inventario_`+sucursal+`.xlsx`);

    Seguridad.formatoRespuesta(1, 200,[{"correcto":1}], 1, res);
    



    let transporter = nodemailer.createTransport({

		service: 'gmail',
			auth: {
		       user: 'grupoa.uniacc@gmail.com',
		       pass: 'grupoauniacc391#.'
	  }
	});
	
	let info = transporter.sendMail({
	    from: 'grupoa.uniacc@gmail.com',
         // list of receivers
		to: 'eoyarcepizarro@gmail.com',
		subject: "Inventario RFID-NEXUS", // Subject line
		html: `<p>Se adjunta Excel con inventario de prueba</p>`, // html body
		attachments: [{filename: nombreExcel, path:pathExcel}]
	});

}
