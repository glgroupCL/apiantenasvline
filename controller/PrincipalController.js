let Seguridad = require('./SeguridadController')
let mysql = require("mysql")
let config = require("../config")
const moment = require('moment');
var storeId;
var datoCliente;

let connection = mysql.createConnection({
    host: config.dbconexion.host,
    user: config.dbconexion.user,
    password: config.dbconexion.pass,
    database: config.dbconexion.dbname
});


/** sa validan los datos de la antena **/
exports.ValidaDatos = (req, res) => {
	
    let {antenaId, mainboardId, popId, channelNumber} = req.body
  
    if(antenaId.length == 15){
        if(mainboardId.length == 12){
            if(popId.length == 12){
                if(channelNumber > 0 && channelNumber <= 20){
                    validaCliente(req.body, res)
                    //Seguridad.formatoRespuesta(1, 200, "succes", 1, res);
                }else{
                    Seguridad.formatoRespuesta(0, 400, "error channel", "El channel debe estar entre 1 y 20", res);
                }
            }else{
                Seguridad.formatoRespuesta(0, 400, "error pop", "No fue posible validar el POPID", res);
            }
        }else{
            Seguridad.formatoRespuesta(0, 400, "error mainboard", "No fue posible validar la mainboard", res);
        }
    }else{
        Seguridad.formatoRespuesta(0, 400, "error antena", "No fue posible validar la antena", res);
    }
}

/* se valida el cliente ingresado*/
function validaCliente (req, res) {
    let {cliente} = req;
  
    if(!connection.connect){
        connection.connect();
    } 
    connection.query("Select concat(SKF198, '|', SKF199, '|', SKF8105) as Kh from SKT24 where SKF199 = (?)",cliente,function (err, rows, fields) {
        if (err) {
			res.status(400).send(err);
        }else{
            let a = rows.length;
            if(a != 0){
                let datos = rows[0].Kh.split("|");
                if(datos[2] == 0){
                    datoCliente = datos[0];
                    validaTienda(req,datoCliente,res);
                }else{
                    Seguridad.formatoRespuesta(0, 400, [], "Cliente eliminado", res);
                }               
            }else{
                Seguridad.formatoRespuesta(0, 400, [], "sin datos", res);
            }    
        }
    });
}

/* se valida la tienda ingresada */
function validaTienda(req, clienteId, res){
    let {store} = req;
    
    if(!connection.connect){
        connection.connect();
    } 
    connection.query("Select concat(SKF107, '|', SKF108 ,'|' ,SKF291) as Md from SKT12 where SKF291 = (?) and SKF108 = (?)",[clienteId, store],function (err, rows, fields) {
        if (err) {
			res.status(400).send(err);
        }else{
             let a = rows.length;
            if(a != 0){
                let datos = rows[0].Md.split("|");
            
                if(datos.length == 3){
                    storeId = datos[0];
                    validarChannel(req,storeId,res)
                
                }else{
                    Seguridad.formatoRespuesta(0, 400, [], "Error cliente", res);
                }               
            }else{
                Seguridad.formatoRespuesta(0, 400, [], "sin datos", res);
            }    
        }
    });
}

/* se valida que el canal esté libre */
function validarChannel(req, storeId, res){
    let {channel} = req;
    if(!connection.connect){
        connection.connect();
    } 
    connection.query("Select concat(SKF246, '|', SKF247, '|', SKF356)As Td from SKT26 where SKF246 = (?) and SKF247 = (?)",[storeId, channel],function (err, rows, fields) {
        if (err) {
			res.status(400).send(err);
        }else{
            let a = rows.length;
            if(a == 0){
                validarPopId(req, res)
            }else{
                Seguridad.formatoRespuesta(0, 400, [], "Channel en uso", res);
            }  
            
        }
    });
} 

/* se valida el popID */
function validarPopId(req, res){
    let {popId} = req;
    if(!connection.connect){
        connection.connect();
    } 
    connection.query("Select SKF8164 from SKT202 where SKF8164 = (?)",popId,function (err, rows, fields) {
        if (err) {
			res.status(400).send(err);
        }else{
            let a = rows.length;
            if(a != 0){
                //si la peticion retorna informacion, se actualiza el pop id con la fecha actual
                actualizaPopId(req, res)
            }else{
                //si no existen datos, se inserta el popid
                insertarPopId(req, res)
            }  
            
        }
    });
}

/* actualizar fecha del popID */
function actualizaPopId(req, res){
    let {popId} = req;
    let fecha = moment().format('YYYY-MM-DD')

    if(!connection.connect){
        connection.connect();
    } 
    connection.query("update SKT202 set SKF8166 = (?) where SKF8164 = (?)",[fecha, popId],function (err, rows, fields) {
        if (err) {
			res.status(400).send(err);
        }else{
            let a = rows.length;
            if(a != 0){
                validaMainBoard(req, res)
            }else{
                Seguridad.formatoRespuesta(0, 400, [], "error al actualizar el popID", res);                
            }  
            
        }
    });
    
}

/* insertar datos del popID */
function insertarPopId(req, res){
    let {popId} = req;
    let fecha = moment().format('YYYY-MM-DD')
   
    if(!connection.connect){
        connection.connect();
    } 
    connection.query("INSERT SKT202(SKF8165,SKF8169,SKF8168,SKF8164,SKF8166) VALUES('数人器',0,1,?,?)",[popId, fecha],function (err, rows, fields) {
        if (err) {
			res.status(400).send(err);
        }else{
            let a = rows.length;
            if(a != 0){
               validaMainBoard(req, res)
            }else{
                Seguridad.formatoRespuesta(0, 400, [], "error al guardar el popID", res);                
            }  
            
        }
    });
}

/* se valida si la mainboard está registrada */
function validaMainBoard(req, res){
    let {mainboardId} = req;
    if(!connection.connect){
        connection.connect();
    } 
    connection.query("Select SKF8164 from SKT202 where SKF8164 = (?)",mainboardId,function (err, rows, fields) {
        if (err) {
			res.status(400).send(err);
        }else{
            let a = rows.length;
            if(a != 0){
                //se actualiza la mainboard
                actualizaMainBoard(req, res)
            }else{
                //se ingresa la mainboard
                insertaMainBoard(req, res)           
            }  
            
        }
    });
}

/* si la mainboard ingresada ya existe, solo se actualiza la fecha */
function actualizaMainBoard(req, res){
    let {mainboardId} = req;
    let fecha = moment().format('YYYY-MM-DD')
    if(!connection.connect){
        connection.connect();
    } 
    connection.query("update SKT202 set SKF8166 = (?) where SKF8164 =  (?)",[fecha,mainboardId],function (err, rows, fields) {
        if (err) {
			res.status(400).send(err);
        }else{
            let a = rows.length;
            if(a != 0){
               //se actualiza correctamente la mainboard
               comprobarExisteAntena(req, res)
            }else{
                //error al actualizar
                Seguridad.formatoRespuesta(0, 400, [], "error al actualizar MAINBOARD", res);           
            }  
            
        }
    });
}


/* si la mainboard no existe se inserta */
function insertaMainBoard(req, res){
    let {mainboardId} = req;
    let fecha = moment().format('YYYY-MM-DD')

    let tipoAntena = validaTipoAntena(req)
    if(tipoAntena != "error"){
        if(!connection.connect){
            connection.connect();
        } 
        connection.query("INSERT SKT202(SKF8165,SKF8169,SKF8168,SKF8164,SKF8166) VALUES((?),0,1,(?),(?))",[tipoAntena,mainboardId,fecha],function (err, rows, fields) {
            if (err) {
                res.status(400).send(err);
            }else{
                let a = rows.length;
                if(a != 0){
                 // se inserta correctamente
                 comprobarExisteAntena(req, res)
                }else{
                   // error al insertar la mainboard
                    Seguridad.formatoRespuesta(0, 400, [], "error al insertar MAINBOARD", res);           
                }  
                
            }
        });
    
    }else{
        Seguridad.formatoRespuesta(0, 400, [], "error en el tipo de antena", res);  
    }
    
}

/* se valida si la antena es AMS o RFS */
function validaTipoAntena(req){

    let tipoAntena;
    let {antenaId} = req;
    if(antenaId.toLowerCase().includes("ams")){
        tipoAntena = "AM";
    }else if (antenaId.toLowerCase().includes("rfs")) {
        tipoAntena = "RF"
    } else {
        tipoAntena = "error"
    }
    return tipoAntena;
    
}

function comprobarExisteAntena(req, res){
    let {antenaId} = req;
    let tipoAntena = validaTipoAntena(req);

    if(tipoAntena != "error"){
        if(!connection.connect){
            connection.connect();
        } 
        connection.query(" Select SKF2 from SKT1 where SKF2 = (?)",antenaId,function (err, rows, fields) {
            if (err) {
                res.status(400).send(err);
            }else{
                console.log(rows)
                let a = rows.length;
                if(a != 0){
                    //existe la antena
                    console.log("existe antena")    
                    actualizaAntena(req, res)
                }else{
                    console.log("no existe antena en el registro")  
                    insertaAntena(req, res, tipoAntena)    
                }  
                
            }
        });
    
    }else{
        Seguridad.formatoRespuesta(0, 400, [], "error en el tipo de antena", res);  
    }
}

/* sin la antena iongresada no se encuentra registrada, se agrega */
function insertaAntena(req, res, tipoAntena){
    let {antenaId, mainboardId, popId} = req;
    let fecha = moment().format('YYYY-MM-DD')
    if(!connection.connect){
        connection.connect();
    } 
    connection.query("INSERT SKT1(SKF12,SKF8203,skf2,SKF95,skf7907,SKF7908,SKF96)VALUES('未使用',1, (?),(?),(?),(?),(?))",[antenaId, tipoAntena, popId, mainboardId, fecha],function (err, rows, fields) {
        if (err) {
            res.status(400).send(err);
        }else{
            console.log(rows)
            let a = rows.length;
            if(a != 0){
               console.log("antena insertada")
               validaLinea(req, res)
            }else{
                Seguridad.formatoRespuesta(0, 400, [], "error al insertar antena", res);
            }  
            
        }
    });
}

/* se la antena se encuentra registrada, se actualizan los datos */
function actualizaAntena(req, res){
    let {antenaId, mainboardId, popId} = req;
    let fecha = moment().format('YYYY-MM-DD')
    if(!connection.connect){
        connection.connect();
    } 
    connection.query("UPDATE SKT1 set SKF8107 = (?),SKF7483=(?),SKF96=(?),skf7907=(?),SKF7908=(?) where SKF2 = (?)",[mainboardId, mainboardId, fecha, popId, mainboardId, antenaId],function (err, rows, fields) {
        if (err) {
            res.status(400).send(err);
        }else{
            console.log(rows)
            let a = rows.length;
            if(a != 0){
               console.log("antena actualizada")
               validaLinea(req, res)
            }else{
                Seguridad.formatoRespuesta(0, 400, [], "error al actualizar antena", res);
            }  
            
        }
    });
}

/** al parecer se valida el pasillo, linea o combinación de datos ya está utilizada -  EN REVISION   **/
function validaLinea(req, res){
    let {antenaId} = req;
    if(!connection.connect){
        connection.connect();
    } 
    connection.query("Select SKF1 from SKT1 where SKF4 = (?) and SKF5 = (?) and SKF206 = 0 and SKF357 = '' and SKF2= (?)",[storeId, datoCliente, antenaId],function (err, rows, fields) {
        if (err) {
            res.status(400).send(err);
        }else{
            console.log(rows)
            let a = rows.length;
            if(a == 0){
               console.log("combinacion de linea disponible")
               insertaLinea(req, res)
            }else{
                Seguridad.formatoRespuesta(0, 400, [], "Linea utilizada", res);
            }  
            
        }
    });
}    

/** se inserta una nueva linea **/
function insertaLinea(req, res){
    let {channel, channelAliases} = req;
    if(!connection.connect){
        connection.connect();
    } 
    connection.query("INSERT SKT26(SKF246,SKF247,SKF356) VALUES((?),(?),(?))",[storeId, channel, channelAliases],function (err, rows, fields) {
        if (err) {
            res.status(400).send(err);
        }else{
            console.log(rows)
            let a = rows.length;
            if(a != 0){
               console.log("linea insertada")
               actualizaLinea(req, res)
            }else{
                Seguridad.formatoRespuesta(0, 400, [], "Linea no insertada", res);
            }  
            
        }
    });
}

/** actualiza otra linea - la verdad nose que hace */
function actualizaLinea(req, res){
    let {antenaId, channel, channelAliases, installationAddress} = req;
    if(!connection.connect){
        connection.connect();
    } 
    connection.query("UPDATE SKT1 set SKF206=(?),SKF357=(?),SKF4=(?),SKF5=(?),SKF12='已使用',SKF3=(?) where SKF2=(?)",[channel, channelAliases, storeId, datoCliente, installationAddress, antenaId],function (err, rows, fields) {
        if (err) {
            res.status(400).send(err);
        }else{
            console.log(rows)
            let a = rows.length;
            if(a != 0){
               console.log("linea actualizada")
               actualizaStore(req, res)
            }else{
                Seguridad.formatoRespuesta(0, 400, [], "Linea no actualizada", res);
            }  
            
        }
    });
}

/** actualizar store */
function actualizaStore(req, res){
    let {antenaId, channel, channelAliases, installationAddress} = req;
    if(!connection.connect){
        connection.connect();
    }
    let numberAux = Number(214)+ Number(channel); 
    let aux = "SKF"
    connection.query("UPDATE SKT12 set SKF? =(?) where SKF291 = (?) and SKF107 = (?)",[Number(numberAux), channel, datoCliente, storeId],function (err, rows, fields) {
        if (err) {
            res.status(400).send(err);
        }else{
            console.log(rows)
            let a = rows.length;
            if(a != 0){
                console.log("tienda actualizada")
                console.log("Será este el fin del hombre araña?")
                almacenaInstalacion(req, res)
                Seguridad.formatoRespuesta(1, 200, "success", 1, res);
            }else{
                Seguridad.formatoRespuesta(0, 400, [], "tienda no actualizada", res);
            }  
            
        }
    });
}

/* se respaldan los datos ingresados en servidor GL */
function almacenaInstalacion(req){
    let fecha = moment().format('YYYY-MM-DD')
    let {tecnico,cliente,store,encargado,contacto,direccion,popId,mainboardId,antenaId, ip} = req;

    sp.exec('spIns_INSTALACION', {
        "fecha" : fecha,
		"tecnico" :tecnico,
		"cliente" :cliente,
		"tienda" :store,
		"encargado" :encargado,
		"contacto" :contacto,
		"direccion" :direccion,
		"pop_ID" :popId,
		"mainboard" :mainboardId,
		"QR_codigo" :antenaId,
		"ip_antena" :ip
    }).then(result=>{
        console.log("Antena Insertada en SQLServer como respaldo")
    
    }).catch(error=>{
        console.log(error);
       
    })

}